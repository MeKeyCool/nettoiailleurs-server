/**
  * Copyright 2018 Pierre-Yves CHRISTMANN & Ciro DE CARO
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  *
  * You may obtain a copy of the License at
  *
  *   http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package local.ailleurs.toi.net

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import local.ailleurs.toi.net.model.ApiRequest
import local.ailleurs.toi.net.router.NatRouter

import scala.concurrent.Await
import scala.util.{Failure, Success}

object Main extends App {

  val host = "0.0.0.0"
  val port = 2121

  implicit val system: ActorSystem = ActorSystem(name = "NetToiAilleurs")
  implicit val mat: ActorMaterializer = ActorMaterializer()

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import io.circe.generic.auto._
  import system.dispatcher

  def route: Route = path("ping") {
    get {
      complete("pong")
    }
  } ~ path("route") {
    post {
      entity(as[ApiRequest]) { body =>
        complete(NatRouter.route(body))
      }
    }
  }

  val binding = Http().bindAndHandle(route, host, port)
  binding.onComplete {
    case Success(_) => println("Success!")
    case Failure(error) => println(s"Failed: ${error.getMessage}")
  }

  import scala.concurrent.duration._
  Await.result(binding, 3.seconds)
}
