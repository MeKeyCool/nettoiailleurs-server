/**
  * Copyright 2018 Pierre-Yves CHRISTMANN & Ciro DE CARO
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  *
  * You may obtain a copy of the License at
  *
  *   http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package local.ailleurs.toi.net.router

import java.util.UUID

import local.ailleurs.toi.net.model._

import scala.concurrent.Future

object NatRouter {
  def route(body: ApiRequest): Future[ApiResponse] = {

    // TODO : manage properly target
    val resourceId = body.request.target.resourceId.get

    // TODO : use body.state.nodeCache
    val routeRequest : Map[Int, Request] = Map( body.context.neighbours map {
      id => {
        id -> body.request.copy(metaData = body.request.metaData.copy(timeLife = body.request.metaData.timeLife + 1))
      }
    } : _* )

    val transmit : Map[UUID, String] = body.state.resources
      .filter( _._1 == resourceId )
      .flatMap(
        p => Map[UUID, String]( p._2.services map { _ -> body.request.data } : _* )
      )

    Future.successful(
      ApiResponse(
        body.state,
        routeRequest,
//        None
        transmit
      )
    )
  }
}
