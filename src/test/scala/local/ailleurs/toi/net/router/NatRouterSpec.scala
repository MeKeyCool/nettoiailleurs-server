/**
 * Copyright 2018 Pierre-Yves CHRISTMANN & Ciro DE CARO
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * 
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package local.ailleurs.toi.net.router

import java.util.UUID

import local.ailleurs.toi.net.model._
import org.scalatest.AsyncWordSpec

class NatRouterSpec extends AsyncWordSpec {

  "NAT router" when {

    "receive a valid ApiRequest" should {

      "transmit request to neighbours" in {


        val apiRequest = ApiRequest(
          Context(
            neighbours = Seq(1),
            services = Map()
          ),
          State(
            nodeCache = None,
            resources = Map()
          ),
          Request(
            id = UUID.randomUUID(),
            metaData = MetaData(
              timeLife = 50,
              lifeTime = 1024,
            ),
            target = Target(
              resourceId = Some(UUID.randomUUID()),
              pagination = None,
              filter = None,
              orderBy = None
            ),
            client = None,
            data = "data"
          )
        )

        val expectedResponse = ApiResponse(
          apiRequest.state,
          routeRequest = Map(
            1 -> apiRequest.request.copy(metaData = apiRequest.request.metaData.copy(timeLife = 51))
          ),
          transmit = Map()
        )

        NatRouter.route(apiRequest) map {
          response => assert(response == expectedResponse)
        }
      }

    }

  }

}
